<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Edit Service Page</title>
</head>
<body>
<jsp:include page="adminMenu.jsp"></jsp:include>
<h3>Edit Service Page</h3>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<%

    String servname = request.getParameter("oldServiceName");
    String servtariff = request.getParameter("oldServiceTariff");
    request.setAttribute("oldServName", servname);

%>

<p>Old Service Name: <%=servname%>
</p>
<p>Old Service Tariff: <%=servtariff%>
</p>

<form method="POST" action="${pageContext.request.contextPath}/editService">
    <input type="hidden" name="oldServName" value="${oldServName}"/>
    <table border="0">
        <tr>
            <td><p>New Service Name</p></td>
            <td><input type="text" name="newServiceName" value="${newServiceName}"/></td>
        </tr>
        <tr>
            <td><p>New Service Tariff</p></td>
            <td><input type="text" name="newServiceTariff" value="${newServiceTariff}"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
                <a href="${pageContext.request.contextPath}/">Cancel</a>
            </td>
        </tr>
    </table>
</form>

<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>