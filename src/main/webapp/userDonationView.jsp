<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Donation View</title>
</head>
<body>
<div class="data">
    <a href="${pageContext.request.contextPath}/donate?locale=en_ENG">ENG</a>
    <a href="${pageContext.request.contextPath}/donate?locale=ru_RU">RUS</a>
</div>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("CLIENT"))) {
        response.sendRedirect("adminHomeView.jsp");
    }
%>
<h3>${donatePageTitle}</h3>

<form method="POST" action="${pageContext.request.contextPath}/donate?email=${user.email}">
    <table border="0">
        <tr>
            <td>${enterTheAmount}</td>
            <td><input type="text" name="donation" value="${donation}"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value=${donateTitle}/>
                <a href="${pageContext.request.contextPath}/userInfo">${cancelbutton}</a>
            </td>
        </tr>
    </table>
</form>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
