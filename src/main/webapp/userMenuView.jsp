<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div style="padding: 5px;">
    <a href="${pageContext.request.contextPath}/serviceList">${serviceListTitle}</a>
    |
    <a href="${pageContext.request.contextPath}/userInfo">${myAccountInfo}</a>
    |
    <a href="${pageContext.request.contextPath}/logout">${logoutTitle}</a>
    |
    <a href="${pageContext.request.contextPath}/donate">${donateTitle}</a>
</div>
