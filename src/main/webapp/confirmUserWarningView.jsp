<%--
  Created by IntelliJ IDEA.
  User: Jacob
  Date: 01.02.2018
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Confirmation Warning</title>
</head>
<body>
<u3>Dear user, please wait for confirmation your account.</u3>
<a href="${pageContext.request.contextPath}/login">Login</a>
</body>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</html>
