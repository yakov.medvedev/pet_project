<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
<%@include file="top.jsp" %>

<h3>Welcome to JACSTELECOM provider.</h3>

<div class="Buttons">
    <a href="${pageContext.request.contextPath}/userRegistration">
        <button>Registrate</button>
    </a>
    <a href="${pageContext.request.contextPath}/userLogin">
        <button>Login</button>
    </a>
</div>

<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>

</body>
</html>
