<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css">
    <title>Login</title>
</head>
<body>
<div class="data">
    <a href="${pageContext.request.contextPath}/userLogin?locale=en_ENG">ENG</a>
    <a href="${pageContext.request.contextPath}/userLogin?locale=ru_RU">RUS</a>
</div>
<%@include file="top.jsp" %>
<h3>${loginPage}</h3>
<%
    Cookie[] cookies = request.getCookies();
    String sessionIDInCookie = "";
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("session_id")) {
            sessionIDInCookie = c.getValue();
        }
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (sessionIDInCookie.equals(session.getId())) {
        if (roleNameCookie.equals("ADMINISTRATOR")) {
            response.sendRedirect("adminHomeView.jsp");
        } else {
            response.sendRedirect("userInfoPage.jsp");
        }
    }
%>
<form method="POST" action="${pageContext.request.contextPath}/userLogin">
    <table border="0">
        <tr>
            <td>${emailField}</td>
            <td><input type="text" name="email" value="${user.email}"/></td>
        </tr>
        <tr>
            <td>${passField}</td>
            <td><input type="password" name="password" value="${user.password}"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value=${submitbutton}>
                <a href="${pageContext.request.contextPath}/">${cancelbutton}</a>
            </td>
        </tr>
    </table>
</form>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
