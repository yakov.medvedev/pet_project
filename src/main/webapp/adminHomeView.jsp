<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Home View</title>

</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<h3>Greetings ${role} ${email}</h3><br>

<jsp:include page="/adminMenu.jsp"></jsp:include>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
