<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Add New Service Error</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<h3>Add New Service Error</h3><br>
<b>Admin, check the correctness of New Service Name or Tariff.</b>
<a href="${pageContext.request.contextPath}/adminAddNewServiceView.jsp">Add New Service</a>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
