<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>

