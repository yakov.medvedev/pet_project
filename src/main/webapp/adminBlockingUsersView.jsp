<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Blocking Users View</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<jsp:include page="adminMenu.jsp"></jsp:include>
<h3>Blocking Users View</h3>
<table border="1" cellpadding="5" cellspacing="1">
    <tr>
        <th>First Name:</th>
        <th>Second Name:</th>
        <th>Email:</th>
        <th>Amount:</th>
        <th>Confirmed:</th>
        <th>Blocked:</th>
        <th>Role:</th>
        <th>Registered:</th>
        <th></th>
    </tr>
    <c:forEach items="${blockUsers}" var="user">
        <tr>
            <td>${user.firstName}</td>
            <td>${user.secondName}</td>
            <td>${user.email}</td>
            <td>${user.amount}</td>
            <td>${user.confirmed}</td>
            <td>${user.blocked}</td>
            <td>${user.role}</td>
            <td>${user.registrationDate}</td>
            <td>
                <form action="blockingUsers?email=${user.email}&block=true" method="post">
                    <input type="submit" value="BLOCK"/>
                </form>
            </td>
            <td>
                <form action="blockingUsers?email=${user.email}&block=false" method="post">
                    <input type="submit" value="UNBLOCK"/>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
