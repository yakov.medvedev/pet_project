<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>
<head>
    <title>Success Page</title>
</head>
<body>
<h3>${successMessage}</h3></br>
Now please <a href="${pageContext.request.contextPath}/userLogin">${loginField}</a>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
