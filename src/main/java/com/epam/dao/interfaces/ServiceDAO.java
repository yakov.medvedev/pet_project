package com.epam.dao.interfaces;

import com.epam.entities.Service;

public interface ServiceDAO extends DAO<Service> {
}
