package com.epam.dao.interfaces;

import java.util.List;

public interface DAO<E> {
    void add(E element);

    void update(String oldElement, E newElement);

    List<E> getAll();

    E get(String name);

    E getById(int elementId);

    void delete(String elementName);
}
