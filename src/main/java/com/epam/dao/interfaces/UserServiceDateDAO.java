package com.epam.dao.interfaces;

public interface UserServiceDateDAO {
    String getStartDate(String email, String serviceName);

    String getEndDate(String email, String serviceName);

    void setStartAndEndDate(String email, String serviceName, String startDate, String endDate);
}
