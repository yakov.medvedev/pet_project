package com.epam.dao.implementation;

import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {

    private Connection connection;

    public UserDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public UserDAOImpl() {
    }

    @Override
    public void add(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("INSERT INTO users (f_name,s_name,email,password,registration_date) VALUES (?,?,?,?,?)");
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getSecondName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getRegistrationDate());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(String oldUser, User newUser) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE users SET f_name=?, s_name=?,email=?,password=?,amount=?,confirmed=?,blocked=?,role=?," +
                            "registration_date=? WHERE email=?");
            statement.setString(1, newUser.getFirstName());
            statement.setString(2, newUser.getSecondName());
            statement.setString(3, newUser.getEmail());
            statement.setString(4, newUser.getPassword());
            statement.setInt(5, newUser.getAmount());
            statement.setBoolean(6, newUser.isConfirmed());
            statement.setBoolean(7, newUser.isBlocked());
            statement.setString(8, newUser.getRole());
            statement.setString(9, newUser.getRegistrationDate());
            statement.setString(10, oldUser);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<User> getAll() {
        List<User> result = null;
        try {
            PreparedStatement st = connection.prepareStatement("SELECT * FROM users");
            ResultSet resultSet = st.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("f_name"),
                        resultSet.getString("s_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getInt("amount"),
                        resultSet.getBoolean("confirmed"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getString("role"),
                        resultSet.getString("registration_date"));
                result.add(user);
            }
            st.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public User get(String email) {
        User user = null;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM users WHERE email=?");
            ps.setString(1, email);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("f_name"),
                        resultSet.getString("s_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getInt("amount"),
                        resultSet.getBoolean("confirmed"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getString("role"),
                        resultSet.getString("registration_date"));
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    public User getById(int elementId) {
        User user = null;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM users WHERE id=?");
            ps.setInt(1, elementId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("f_name"),
                        resultSet.getString("s_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getInt("amount"),
                        resultSet.getBoolean("confirmed"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getString("role"),
                        resultSet.getString("registration_date"));
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    public void delete(String email) {
        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM users WHERE email=?");
            ps.setString(1, email);
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
