package com.epam.connection;

import org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class Pool {

    private static String url;
    private static String username;
    private static String password;
    private static String driver;
    private static Connection connection;
    private static DataSource ds;
    private static Logger LOGGER = LoggerFactory.getLogger(Pool.class);

    static {
        Properties props = new Properties();
        try (InputStream is = Pool.class.getResourceAsStream("/db.properties")) {
            props.load(is);
            Class.forName("org.mariadb.jdbc.Driver");
            driver = props.getProperty("driver");
            url = props.getProperty("url");
            username = props.getProperty("username");
            password = props.getProperty("password");
            DriverAdapterCPDS cpds = new DriverAdapterCPDS();
            cpds.setDriver(driver);
            cpds.setUrl(url);
            cpds.setUser(username);
            cpds.setPassword(password);
            SharedPoolDataSource tds = new SharedPoolDataSource();
            tds.setConnectionPoolDataSource(cpds);
            tds.setMaxTotal(20);
            tds.setDefaultMaxWaitMillis(1000);
            ds = tds;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        try {
            connection = ds.getConnection();
        } catch (SQLException e) {
            LOGGER.error("Connection failed.");
            e.printStackTrace();
        }
        return connection;
    }
}
