package com.epam.entities;

public class Service {

    private int serviceID;
    private String serviceName;
    private int tariff;

    public Service(String serviceName, int tariff) {
        this.serviceName = serviceName;
        this.tariff = tariff;
    }

    public Service(int serviceID, String serviceName, int tariff) {
        this.serviceID = serviceID;
        this.serviceName = serviceName;
        this.tariff = tariff;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getTariff() {
        return tariff;
    }

    public void setTariff(int tariff) {
        this.tariff = tariff;
    }

    @Override
    public String toString() {
        return "Service Name: '" + serviceName + '\'' +
                ", tariff: " + tariff +
                '.';
    }
}
