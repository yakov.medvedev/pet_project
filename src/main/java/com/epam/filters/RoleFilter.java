package com.epam.filters;

import javax.servlet.*;
import java.io.IOException;

//@WebFilter(filterName = "role_filter.RoleFilter", urlPatterns = {"/*"})
public class RoleFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {


        filterChain.doFilter(servletRequest, servletResponse);
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        String servletPath = request.getServletPath();
//        RequestDispatcher dispatcher = null;
//
//        HttpSession session = request.getSession();
//        String role = (String) session.getAttribute("role");
//
//        List<String> adminPages = new ArrayList<String>();
//        adminPages.add("/admin");
//        if (role == null) {
//            dispatcher = request.getRequestDispatcher("/setRole");
//            dispatcher.forward(servletRequest, servletResponse);
//        }
//
//        if (role.equals("ADMIN")) {
//            for (String s : adminPages) {
//                if (servletPath.equals(s)) {
//                    dispatcher = request.getRequestDispatcher(servletPath);
//                    dispatcher.forward(servletRequest, servletResponse);
//                } else {
//                    dispatcher = request.getRequestDispatcher("/permissionDeniedView.jsp");
//                    dispatcher.forward(servletRequest, servletResponse);
//                }
//            }
//        }
    }

    public void destroy() {
    }
}
