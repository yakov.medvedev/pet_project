package com.epam.services.download;

import com.epam.servlets.LoginServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;

public class DownloadHtmlFileService implements FileDownload {
    private static Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
    private Connection connection;

    public DownloadHtmlFileService(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void downloadFile() {
    }

    @Override
    public void downloadFile(String url) throws MalformedURLException {
        String line;
        URL myurl = new URL(url);
        String pathToDownloads = System.getProperty("user.home") + "\\Downloads\\services.html";
        try (InputStream inputStream = myurl.openStream();
             BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
             BufferedWriter bw = new BufferedWriter(new FileWriter(pathToDownloads, true))) {
            while ((line = br.readLine()) != null) {
                bw.write(line);
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}