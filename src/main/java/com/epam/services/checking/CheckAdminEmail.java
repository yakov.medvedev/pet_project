package com.epam.services.checking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckAdminEmail {
    private static Connection connection;

    public CheckAdminEmail(Connection connection) {
        CheckAdminEmail.connection = connection;
    }

    public static boolean check(String email) {
        boolean result = false;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("SELECT COUNT(*) FROM users WHERE email=? AND role='ADMINISTRATOR'");
            ps.setString(1, email);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getInt(1) >= 1) {
                    result = true;
                }
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
