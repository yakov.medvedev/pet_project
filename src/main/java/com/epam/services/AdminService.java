package com.epam.services;

import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AdminService {
    private Connection connection;
    private UserDAO userDAO;
    private ServiceDAO serviceDAO;

    public AdminService(Connection connection) {
        this.connection = connection;
        this.userDAO = new UserDAOImpl(connection);
        this.serviceDAO = new ServiceDAOImpl(connection);
    }

    public List<User> getAdministrators() {
        List<User> result = null;
        try {
            Statement st = connection.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT email FROM users WHERE role like 'ADMINISTRATOR'");
            result = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("f_name"),
                        resultSet.getString("s_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getInt("amount"),
                        resultSet.getBoolean("confirmed"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getString("role"),
                        resultSet.getString("registration_date"));
                result.add(user);
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void confirm(String userEmail) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE users SET confirmed=TRUE WHERE email=?");
            statement.setString(1, userEmail);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void block(String userEmail) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE users SET blocked=? WHERE email=?");
            statement.setBoolean(1, true);
            statement.setString(2, userEmail);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void unblock(String userEmail) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE users SET blocked=? WHERE email=?");
            statement.setBoolean(1, false);
            statement.setString(2, userEmail);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllUnregistered() {
        List<User> result = null;
        try {
            Statement st = connection.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM users WHERE confirmed=FALSE");
            result = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("f_name"),
                        resultSet.getString("s_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getInt("amount"),
                        resultSet.getBoolean("confirmed"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getString("role"),
                        resultSet.getString("registration_date"));
                result.add(user);
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<User> getAllBlocked() {
        List<User> result = null;
        try {
            Statement st = connection.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM users WHERE blocked=TRUE ");
            result = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("f_name"),
                        resultSet.getString("s_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getInt("amount"),
                        resultSet.getBoolean("confirmed"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getString("role"),
                        resultSet.getString("registration_date"));
                result.add(user);
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}