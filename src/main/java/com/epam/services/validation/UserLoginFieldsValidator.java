package com.epam.services.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserLoginFieldsValidator {

    private static int recommendPassLength = 4;

    public UserLoginFieldsValidator() {
    }

    public static boolean validate(String eMail, String pass) {
        return checkIsThisEmail(eMail) && checkPasswordLength(pass);
    }

    public static boolean validate(String eMail, String pass, String fname, String sname) {
        boolean emailCheck = checkIsThisEmail(eMail);
        if (emailCheck) {
            if (checkPasswordLength(pass)) {
                return checkFnameAndSname(fname, sname);
            }
        }
        return false;
    }

    private static boolean checkFnameAndSname(String fname, String sname) {
        boolean fnameLength = fname.length() > 0;
        boolean snameLength = sname.length() > 0;
        boolean fnameLetters = checkContainsOnlyLetters(fname);
        boolean snameLetters = checkContainsOnlyLetters(sname);
        return (fnameLength && snameLength && fnameLetters && snameLetters);
    }

    private static boolean checkIsThisEmail(String eMail) {
        Pattern pat = Pattern.compile("^.+@[a-z]+\\.[a-z]{2,5}$");
        Matcher matcher = pat.matcher(eMail);
        return matcher.find();
    }

    private static boolean checkPasswordLength(String pass) {
        return pass.length() > recommendPassLength;
    }

    private static boolean checkContainsOnlyLetters(String field) {
        Pattern pat = Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pat.matcher(field);
        return matcher.find();
    }

    public static boolean validateNumberField(String donationInString) {
        Pattern pat = Pattern.compile("\\d+");
        Matcher matcher = pat.matcher(donationInString);
        return matcher.find();
    }

    public static boolean validateServiceName(String serviceName) {
        Pattern pat = Pattern.compile("^[a-zA-Zа-яА-Я0-9]+[ a-zA-Zа-яА-Я0-9]*$");
        Matcher matcher = pat.matcher(serviceName);
        return matcher.find();
    }

    public int getRecommendPassLength() {
        return recommendPassLength;
    }

    public void setRecommendPassLength(int recommendPassLength) {
        UserLoginFieldsValidator.recommendPassLength = recommendPassLength;
    }
}
