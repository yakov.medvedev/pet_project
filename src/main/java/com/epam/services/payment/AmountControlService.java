package com.epam.services.payment;

import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.implementation.UserServiceDateDAOImpl;
import com.epam.dao.implementation.UserServiceListDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.dao.interfaces.UserServiceDateDAO;
import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;
import com.epam.entities.User;
import com.epam.services.AdminService;
import com.epam.services.checking.CheckUserBlocked;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AmountControlService {


    private Connection connection;
    private String userEmail;
    private User user;
    private AdminService adminService;
    private UserDAO userservice;
    private UserServiceListDAO userserviceListDAO;
    private UserServiceDateDAO userServiceDateDAO;
    private List<Service> userServicesList;

    public AmountControlService(Connection connection) throws SQLException {
        this.connection = connection;
        this.adminService = new AdminService(connection);
        this.userservice = new UserDAOImpl(connection);
        this.userserviceListDAO = new UserServiceListDAOImpl(connection);
        this.userServiceDateDAO = new UserServiceDateDAOImpl(connection);
    }

    public AmountControlService(String userEmail, Connection connection) throws SQLException {
        this.connection = connection;
        this.userEmail = userEmail;
        this.adminService = new AdminService(connection);
        this.userservice = new UserDAOImpl(connection);
        this.userserviceListDAO = new UserServiceListDAOImpl(connection);
        this.userServiceDateDAO = new UserServiceDateDAOImpl(connection);
    }

    public void run() throws ParseException {
        initUser();
        initUserServicesList();
        for (Service serv : userServicesList) {
            if (isBlocked()) {
                checkAmountAndUpdateUser(serv);
            } else {
                if (!(isCurrentDateBeforeEndDate(serv))) {
                    checkAmountAndUpdateUser(serv);
                }
            }
        }
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    private void initUser() {
        this.user = userservice.get(userEmail);
    }

    private void initUserServicesList() {
        this.userServicesList = userserviceListDAO.getAllServiceToUser(userEmail);
    }

    private void checkAmountAndUpdateUser(Service service) {
        if (!(user.getAmount() - service.getTariff() < 0)) {
            adminService.unblock(user.getEmail());
            user.setBlocked(false);
            wrightNewServiceDates(service);
            user.setAmount(user.getAmount() - service.getTariff());
            userservice.update(user.getEmail(), user);
        } else {
            user.setBlocked(true);
            userservice.update(user.getEmail(), user);
        }
    }

    private boolean isCurrentDateBeforeEndDate(Service service) throws ParseException {
        String pattern = "dd.MM.yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date currentDate = new Date();
        String end = userServiceDateDAO.getEndDate(user.getEmail(), service.getServiceName());
        Date endTermDate = simpleDateFormat.parse(end);
        return currentDate.before(endTermDate);
    }


    private boolean isBlocked() {
        new CheckUserBlocked(connection);
        return CheckUserBlocked.check(user.getEmail(), user.getPassword());
    }

    private String getStringDate(Date date) {
        String pattern = "dd.MM.yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    private void wrightNewServiceDates(Service service) {
        Date currentDate = new Date();
        String newStartDate = getStringDate(currentDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.MINUTE, 3);
        Date endDate = calendar.getTime();
        String newEndDate = getStringDate(endDate);
        userServiceDateDAO.setStartAndEndDate(user.getEmail(), service.getServiceName(), newStartDate, newEndDate);
    }
}
