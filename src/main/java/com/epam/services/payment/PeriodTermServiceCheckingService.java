package com.epam.services.payment;

import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.TimerTask;

public class PeriodTermServiceCheckingService extends TimerTask {
    private UserDAO userDAO;
    private AmountControlService controlService;

    public PeriodTermServiceCheckingService(Connection connection) throws SQLException {
        this.userDAO = new UserDAOImpl(connection);
        this.controlService = new AmountControlService(connection);
    }

    @Override
    public void run() {
        List<User> userList = userDAO.getAll();
        if (!(userList == null)) {
            for (User user : userList) {
                controlService.setUserEmail(user.getEmail());
                try {
                    controlService.run();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}