package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import com.epam.services.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/getAllUsers")
public class AdminGetAllUsersServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminGetAllUsersServlet.class);
    private UserDAO userDAOService;
    private AdminService adminService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try (Connection connection = Pool.getConnection()) {
            userDAOService = new UserDAOImpl(connection);
            adminService = new AdminService(connection);
            List<User> blockUsers = userDAOService.getAll();
            HttpSession session = req.getSession();
            session.setAttribute("allUsers", blockUsers);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/adminAllUsersListView.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String block = req.getParameter("block");
        try (Connection connection = Pool.getConnection()) {
            adminService = new AdminService(connection);
            if (block.equals("true")) {
                adminService.block(req.getParameter("email"));
                LOGGER.info("User " + req.getParameter("email") + " was blocked.");
            } else {
                adminService.unblock(req.getParameter("email"));
                LOGGER.info("User " + req.getParameter("email") + " was unblocked.");
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/adminHomeView.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
