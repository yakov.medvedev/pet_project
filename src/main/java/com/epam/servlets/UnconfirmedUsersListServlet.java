package com.epam.servlets;


import com.epam.connection.Pool;
import com.epam.entities.User;
import com.epam.services.AdminService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/unconfirmedUsers")
public class UnconfirmedUsersListServlet extends HttpServlet {
    private AdminService service = new AdminService(Pool.getConnection());

    public UnconfirmedUsersListServlet() throws SQLException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> unconfirmedUsers = service.getAllUnregistered();
        HttpSession session = req.getSession();
        session.setAttribute("unconfirmedUsers", unconfirmedUsers);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/adminUnconfirmedUsersView.jsp");
        dispatcher.forward(req, resp);
    }
}
