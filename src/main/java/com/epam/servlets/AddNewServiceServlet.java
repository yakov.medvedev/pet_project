package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.entities.Service;
import com.epam.services.validation.UserLoginFieldsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/addNewService")
public class AddNewServiceServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(AddNewServiceServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newServiceName = req.getParameter("newServiceName");
        String newServiceTariff = req.getParameter("newServiceTariff");
        boolean validServiceName = UserLoginFieldsValidator.validateServiceName(newServiceName);
        boolean validServiceTariff = UserLoginFieldsValidator.validateNumberField(newServiceTariff);
        RequestDispatcher dispatcher;
        if (validServiceName && validServiceTariff) {
            Service newService = new Service(newServiceName, Integer.valueOf(newServiceTariff));
            Connection connection = Pool.getConnection();
            ServiceDAO serviceDAO = new ServiceDAOImpl(connection);
            serviceDAO.add(newService);
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            LOGGER.info("Service " + newServiceName + " tariff " + newServiceTariff + " added.");
            dispatcher = req.getServletContext().getRequestDispatcher("/adminHomeView.jsp");
            dispatcher.forward(req, resp);
        } else {
            LOGGER.error("Service " + newServiceName + " tariff " + newServiceTariff + " - ERROR. Service not added.");
            dispatcher = req.getServletContext().getRequestDispatcher("/adminAddNewServiceErrorView.jsp");
            dispatcher.forward(req, resp);
        }
    }
}

