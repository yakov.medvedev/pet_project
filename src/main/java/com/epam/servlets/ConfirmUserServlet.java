package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.services.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/confirmUser")
public class ConfirmUserServlet extends HttpServlet {
    private static Logger LOGGER = LoggerFactory.getLogger(ConfirmUserServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        AdminService service = new AdminService(Pool.getConnection());
        String email = req.getParameter("email");
        service.confirm(email);
        LOGGER.info("User " + email + " confirmed.");
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/unconfirmedUsers");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}

