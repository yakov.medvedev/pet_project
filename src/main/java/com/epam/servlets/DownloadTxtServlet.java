package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.services.download.DownloadTxtFileService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/downloadTxt")
public class DownloadTxtServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DownloadTxtFileService downloadTxtFileService;
        downloadTxtFileService = new DownloadTxtFileService(Pool.getConnection());
        downloadTxtFileService.downloadFile();
        RequestDispatcher dispatcher = req.getServletContext()
                .getRequestDispatcher("/userInfoPage.jsp");
        dispatcher.forward(req, resp);
    }
}
