package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.UserServiceListDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/userDeleteService")
public class UserDeleteServiceServlet extends HttpServlet {
    private static Logger LOGGER = LoggerFactory.getLogger(UserDeleteServiceServlet.class);
    private UserServiceListDAOImpl userServiceListDAO =
            new UserServiceListDAOImpl(Pool.getConnection());

    public UserDeleteServiceServlet() throws SQLException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String serviceName = req.getParameter("serviceName");
        String email = req.getParameter("email");
        userServiceListDAO.removeServiceFromUser(email, serviceName);
        LOGGER.info("User " + email + " deleted Service " + serviceName);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/userInfoPage.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
}
