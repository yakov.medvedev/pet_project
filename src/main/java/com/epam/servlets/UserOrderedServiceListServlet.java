package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.UserServiceListDAOImpl;
import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@WebServlet(urlPatterns = "/getOrderedList")
public class UserOrderedServiceListServlet extends HttpServlet {
    private UserServiceListDAO serviceListDAO = new UserServiceListDAOImpl(Pool.getConnection());

    public UserOrderedServiceListServlet() throws SQLException {
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<Service> serviceList;
        if (req.getParameter("order").equals("byName")) {
            serviceList = serviceListDAO.getAllServiceOrderedByName();
        } else {
            serviceList = serviceListDAO.getAllServiceOrderedByPrice();
        }
        session.setAttribute("servicesList", serviceList);
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/servicesList.jsp");
        dispatcher.forward(req, resp);
    }
}

