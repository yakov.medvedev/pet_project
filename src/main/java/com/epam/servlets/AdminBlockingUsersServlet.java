package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.entities.User;
import com.epam.services.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/blockingUsers")
public class AdminBlockingUsersServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminBlockingUsersServlet.class);
    private AdminService service = new AdminService(Pool.getConnection());

    public AdminBlockingUsersServlet() throws SQLException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> blockUsers = service.getAllBlocked();
        HttpSession session = req.getSession();
        session.setAttribute("blockUsers", blockUsers);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/adminBlockingUsersView.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String block = req.getParameter("block");
        if (block.equals("true")) {
            service.block(req.getParameter("email"));
            LOGGER.info("User " + req.getParameter("email") + " was blocked.");
        } else {
            service.unblock(req.getParameter("email"));
            LOGGER.info("User " + req.getParameter("email") + " was unblocked.");
        }
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/adminBlockingUsersView.jsp");
        dispatcher.forward(req, resp);
    }
}
