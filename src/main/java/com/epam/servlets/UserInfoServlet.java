package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.implementation.UserServiceListDAOImpl;
import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;
import com.epam.entities.User;
import com.epam.util.LocalizationService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/userInfo")
public class UserInfoServlet extends HttpServlet {
    private UserDAOImpl userDAO;
    private UserServiceListDAO userServiceListDAO;

    public UserInfoServlet() {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.setStatus(HttpServletResponse.SC_OK);
        HttpSession session = req.getSession();
        String email = (String) session.getAttribute("email");
        try (Connection connection = Pool.getConnection()) {
            userDAO = new UserDAOImpl(connection);
            User user = userDAO.get(email);
            userServiceListDAO = new UserServiceListDAOImpl(connection);
            List<Service> userServiceList = userServiceListDAO.getAllServiceToUser(user.getEmail());
            session.setAttribute("user", user);
            session.setAttribute("services", userServiceList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        LocalizationService localizationService;
        String localizationParam = req.getParameter("locale");
        if (localizationParam == null) {
            localizationService = new LocalizationService(req.getLocale());
        } else {
            localizationService = new LocalizationService(localizationParam);
        }

        session.setAttribute("helloTitle", localizationService.getLocaleString("helloTitle"));
        session.setAttribute("fNameField", localizationService.getLocaleString("fNameField"));
        session.setAttribute("sNameField", localizationService.getLocaleString("sNameField"));
        session.setAttribute("amountField", localizationService.getLocaleString("amountField"));
        session.setAttribute("blockedTitle", localizationService.getLocaleString("blockedTitle"));
        session.setAttribute("registrationDateTitle", localizationService.getLocaleString("registrationDateTitle"));
        session.setAttribute("connectedServicesTitle", localizationService.getLocaleString("connectedServicesTitle"));
        session.setAttribute("serviceNameTitle", localizationService.getLocaleString("serviceNameTitle"));
        session.setAttribute("tariffTitle", localizationService.getLocaleString("tariffTitle"));
        session.setAttribute("deleteButton", localizationService.getLocaleString("deleteButton"));


        session.setAttribute("serviceListTitle", localizationService.getLocaleString("serviceListTitle"));
        session.setAttribute("myAccountInfo", localizationService.getLocaleString("myAccountInfo"));
        session.setAttribute("logoutTitle", localizationService.getLocaleString("logoutTitle"));
        session.setAttribute("donateTitle", localizationService.getLocaleString("donateTitle"));

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/userInfoPage.jsp");
        dispatcher.forward(req, resp);
    }
}
