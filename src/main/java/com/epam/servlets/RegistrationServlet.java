package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import com.epam.services.checking.CheckUserInDB;
import com.epam.services.validation.UserLoginFieldsValidator;
import com.epam.util.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/userRegistration")
public class RegistrationServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(RegistrationServlet.class);
    private UserDAO userDAO = new UserDAOImpl(Pool.getConnection());

    public RegistrationServlet() {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String fname = request.getParameter("f_name");
        String sname = request.getParameter("s_name");
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        boolean valid = UserLoginFieldsValidator.validate(email, pass, fname, sname);
        boolean isPresent = false;

        new CheckUserInDB(Pool.getConnection());
        isPresent = CheckUserInDB.check(email, pass);

        RequestDispatcher dispatcher;

        if (!valid) {
            LOGGER.warn("Incorrect user input.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            LOGGER.warn(" Redirect to validation error page.");
            dispatcher = this.getServletContext().getRequestDispatcher("/validError.jsp");
            dispatcher.forward(request, response);
        } else if (isPresent) {
            dispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
            dispatcher.forward(request, response);
        } else {
            User user = new User(fname, sname, email, pass);
            userDAO.add(user);
        }
        LOGGER.info("User " + fname + " " + sname + " " + email + " was successfully added.");
        dispatcher = this.getServletContext().getRequestDispatcher("/successMsg");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LocalizationService localizationService;
        String localizationParam = request.getParameter("locale");
        if (localizationParam == null) {
            localizationService = new LocalizationService(request.getLocale());
        } else {
            localizationService = new LocalizationService(localizationParam);
        }
        session.setAttribute("fNameField", localizationService.getLocaleString("fNameField"));
        session.setAttribute("sNameField", localizationService.getLocaleString("sNameField"));
        session.setAttribute("emailField", localizationService.getLocaleString("emailField"));
        session.setAttribute("passField", localizationService.getLocaleString("passField"));
        session.setAttribute("submitbutton", localizationService.getLocaleString("submitbutton"));
        session.setAttribute("cancelbutton", localizationService.getLocaleString("cancelbutton"));
        session.setAttribute("regpage", localizationService.getLocaleString("regpage"));
        session.setAttribute("successMessage", localizationService.getLocaleString("successMessage"));
        session.setAttribute("profileInfoTitle", localizationService.getLocaleString("profileInfoTitle"));
        session.setAttribute("loginField", localizationService.getLocaleString("loginField"));

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/registration.jsp");
        dispatcher.forward(request, response);
    }
}