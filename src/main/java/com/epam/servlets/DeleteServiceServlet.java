package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.ServiceDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/deleteService")
public class DeleteServiceServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(DeleteServiceServlet.class);
    private ServiceDAOImpl serviceDAO = new ServiceDAOImpl(Pool.getConnection());

    public DeleteServiceServlet() throws SQLException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        serviceDAO.delete(req.getParameter("serviceName"));
        LOGGER.info("Service" + req.getParameter("serviceName") + " deleted.");
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/adminServiceList.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
