package com.epam.dao.implementation;

import com.epam.connection.PoolH2DB;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserDAOImplTest {
    private Connection connection = PoolH2DB.getConnection();
    private UserDAO userService = new UserDAOImpl(connection);

    @Before
    public void clearTableBefore() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("TRUNCATE TABLE users");
        ps.execute();
    }

    @Test
    public void saveUserTest() throws SQLException {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        userService.add(u1);
        User u2 = userService.get(u1.getEmail());
        assertEquals(u1.getEmail(), u2.getEmail());
    }

    @Test
    public void getUserTest() throws SQLException {
        User u1 = new User("Rick", "Ivanov", "foo@gmail.com", "333333");
        userService.add(u1);
        User u2 = userService.get(u1.getEmail());
        assertEquals(u1.getEmail(), u2.getEmail());
    }

    @Test
    public void getAllUsersTest() throws SQLException {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        User u2 = new User("Rick", "Ivanov", "foo@gmail.com", "333333");
        User u3 = new User("Petro", "Smoliy", "bar@gmail.com", "111111");
        userService.add(u1);
        userService.add(u2);
        userService.add(u3);
        List<User> list = userService.getAll();
        assertEquals(3, list.size());
    }

    @Test
    public void deleteUserTest() throws SQLException {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        User u2 = new User("Rick", "Ivanov", "foo@gmail.com", "333333");
        User u3 = new User("Petro", "Smoliy", "bar@gmail.com", "111111");
        userService.add(u1);
        userService.add(u2);
        userService.add(u3);
        userService.delete(u1.getEmail());
        List<User> list = userService.getAll();
        assertNull(userService.get(u1.getEmail()));
    }

    @After
    public void clearTable() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("TRUNCATE TABLE users");
        ps.execute();
        ps.close();
    }
}