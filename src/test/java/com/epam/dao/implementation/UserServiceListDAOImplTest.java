package com.epam.dao.implementation;

import com.epam.connection.PoolH2DB;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.dao.interfaces.UserDAO;
import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;
import com.epam.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserServiceListDAOImplTest {

    private static Connection connection = PoolH2DB.getConnection();
    private UserDAO users = new UserDAOImpl(connection);
    private ServiceDAO services = new ServiceDAOImpl(connection);
    private UserServiceListDAO usersList = new UserServiceListDAOImpl(connection);

    public UserServiceListDAOImplTest() throws SQLException {
    }

    @BeforeClass
    public static void setReferentialIntegrity() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SET REFERENTIAL_INTEGRITY FALSE");
        ps.execute();
        ps.close();
    }

    @Before
    public void clearTableBefore() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("truncate table USERS_SERVICES");
            ps.execute("truncate table SERVICES");
            ps.execute("truncate table USERS");
        }
    }

    @Test
    public void addServiceToUser() throws Exception {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        Service s1 = new Service("New Service", 220);
        users.add(u1);
        services.add(s1);
        usersList.addServiceToUser(u1.getEmail(), s1.getServiceName());
        assertEquals(s1.getServiceName(), usersList.getServiceToUser(u1.getEmail(), s1.getServiceName()).getServiceName());
    }

    @Test
    public void removeServiceFromUser() throws Exception {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        Service s1 = new Service("New Service", 220);
        users.add(u1);
        services.add(s1);
        usersList.removeServiceFromUser(u1.getEmail(), s1.getServiceName());
        assertNull(usersList.getServiceToUser(u1.getEmail(), s1.getServiceName()));
    }

    @Test
    public void getAllServiceToUser() throws Exception {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        Service s1 = new Service("New Service", 220);
        Service s2 = new Service("Cool Service", 111);
        Service s3 = new Service("X Service", 333);
        users.add(u1);
        services.add(s1);
        services.add(s2);
        services.add(s3);
        usersList.addServiceToUser(u1.getEmail(), s1.getServiceName());
        usersList.addServiceToUser(u1.getEmail(), s2.getServiceName());
        usersList.addServiceToUser(u1.getEmail(), s3.getServiceName());
        List<Service> list = usersList.getAllServiceToUser(u1.getEmail());
        assertEquals(3, list.size());
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("truncate table USERS_SERVICES");
            ps.execute("truncate table SERVICES");
            ps.execute("truncate table USERS");
        }
    }
}
