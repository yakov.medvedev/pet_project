package com.epam.dao.implementation;

import com.epam.connection.PoolH2DB;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.dao.interfaces.UserDAO;
import com.epam.dao.interfaces.UserServiceDateDAO;
import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;
import com.epam.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;

public class UserServiceDateDAOImplTest {

    private static Connection connection = PoolH2DB.getConnection();
    private UserDAO users = new UserDAOImpl(connection);
    private ServiceDAO services = new ServiceDAOImpl(connection);
    private UserServiceDateDAO dateService = new UserServiceDateDAOImpl(connection);
    private UserServiceListDAO usersList = new UserServiceListDAOImpl(connection);

    public UserServiceDateDAOImplTest() throws SQLException {
    }

    @BeforeClass
    public static void setReferentialIntegrity() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SET REFERENTIAL_INTEGRITY FALSE");
        ps.execute();
        ps.close();
    }

    @Before
    public void clearTableBefore() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("truncate table USERS_SERVICES");
            ps.execute("truncate table SERVICES");
            ps.execute("truncate table USERS");
        }
    }

    @Test
    public void setStartAndEndDate() throws Exception {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        Service s1 = new Service("New Service", 220);
        users.add(u1);
        services.add(s1);
        usersList.addServiceToUser(u1.getEmail(), s1.getServiceName());
        String startDate = "01.02.2018";
        String endDate = "01.03.2018";
        dateService.setStartAndEndDate(u1.getEmail(), s1.getServiceName(), startDate, endDate);
        assertEquals(startDate, dateService.getStartDate(u1.getEmail(), s1.getServiceName()));
    }

    @Test
    public void getStartDate() throws Exception {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        Service s1 = new Service("New Service", 220);
        users.add(u1);
        services.add(s1);
        usersList.addServiceToUser(u1.getEmail(), s1.getServiceName());
        String startDate = "01.02.2018";
        String endDate = "01.03.2018";
        dateService.setStartAndEndDate(u1.getEmail(), s1.getServiceName(), startDate, endDate);
        assertEquals(startDate, dateService.getStartDate(u1.getEmail(), s1.getServiceName()));
    }

    @Test
    public void getEndDate() throws Exception {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        Service s1 = new Service("New Service", 220);
        users.add(u1);
        services.add(s1);
        usersList.addServiceToUser(u1.getEmail(), s1.getServiceName());
        String startDate = "01.02.2018";
        String endDate = "01.03.2018";
        dateService.setStartAndEndDate(u1.getEmail(), s1.getServiceName(), startDate, endDate);
        assertEquals(endDate, dateService.getEndDate(u1.getEmail(), s1.getServiceName()));
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("truncate table USERS_SERVICES");
            ps.execute("truncate table SERVICES");
            ps.execute("truncate table USERS");
        }
    }
}