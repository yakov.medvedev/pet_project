package com.epam.services.checking;

import com.epam.connection.PoolH2DB;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertTrue;

public class CheckUserInDBTest {

    private Connection connection = PoolH2DB.getConnection();
    private UserDAO userService = new UserDAOImpl(connection);

    public CheckUserInDBTest() throws SQLException {
    }

    @Before
    public void clearTableBefore() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE USERS");
        }
    }

    @Test
    public void checkIfUserAlreadyExistInDB() throws SQLException {
        User u1 = new User("Gary", "Oldman", "oldman@mail.ru", "222222");
        userService.add(u1);
        new CheckUserInDB(connection);
        assertTrue(CheckUserInDB.check("oldman@mail.ru", "222222"));
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE USERS");
        }
    }
}